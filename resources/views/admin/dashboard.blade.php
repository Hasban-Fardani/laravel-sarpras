@extends('layouts.header')
@section('content')
    @can('isAdmin')
    <button class="btn btn-outline-primary col-lg-12">Ini akses Admin</button>
    @elsecan('isOperator')
    <button class="btn btn-outline-success col-lg-12">Ini akses Operator</button>
    @endcan
@endsection
