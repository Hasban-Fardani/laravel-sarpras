@extends('layouts.header')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">
                    Lending Request
                </h5>
            </div>
            <div class="card-body">

                <div id="table1_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div id="table1_filter" class="dataTables_filter">
                                <label>Search:<input type="search" class="form-control form-control-sm" placeholder=""
                                        aria-controls="table1">
                                    <br>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row dt-row">
                        <div class="col-sm-12">
                            <table class="table dataTable no-footer" id="table1" aria-describedby="table1_info">
                                <thead>
                                    <tr>
                                        <th class="sorting sorting_asc" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-sort="ascending"
                                            aria-label="Name: activate to sort column descending" style="width: 72.1875px;">
                                            Name</th>
                                        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-label="Phone: activate to sort column ascending"
                                            style="width: 126.688px;">Item</th>
                                        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-label="City: activate to sort column ascending"
                                            style="width: 136.516px;">Count</th>
                                        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-label="City: activate to sort column ascending"
                                            style="width: 136.516px;">Date</th>
                                        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-label="Email: activate to sort column ascending"
                                            style="width: 364.781px;">Description</th>
                                        <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1"
                                            colspan="1" aria-label="Status: activate to sort column ascending"
                                            style="width: 60.8281px;">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd">
                                        <td class="sorting_1">-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td class="d-flex">
                                            <button type="submit" class="btn btn-outline-success me-1 mb-1 ">Accept</button>
                                            <button type="submit" class="btn btn-outline-danger me-1 mb-1 ">Deny</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing
                                1 to 25 of 26 entries</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
