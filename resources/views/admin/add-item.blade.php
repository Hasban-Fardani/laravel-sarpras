@extends('layouts.header')
@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Add Item</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form class="form">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="first-name-column">Item name</label>
                                    <input type="text" id="first-name-column" class="form-control" placeholder="" name="name">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="last-name-column">Brand</label>
                                    <input type="text" id="last-name-column" class="form-control" placeholder="" name="merk">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="city-column">Series number</label>
                                    <input type="text" id="city-column" class="form-control" placeholder="" name="no_seri">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="country-floating">Size</label>
                                    <input type="number" id="country-floating" class="form-control" name="size" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="company-column">Material</label>
                                    <select class="col-md-8 form-group form-select form-control" id="basicSelect">
                                        <option></option>
                                        <option value="contoh" >Contoh 1</option>
                                        <option>Contoh 2    </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="email-id-column">Category</label>
                                    <select class="col-md-8 form-group form-select form-control" id="basicSelect">
                                        <option></option>
                                        <option value="contoh" >Contoh 1</option>
                                        <option>Contoh 2    </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="first-name-column">Created year</label>
                                    <input type="datetime-local" id="contact-info-horizontal" class="form-control mb-2" name="created_years" >
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="first-name-column">Image</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group mb-2">
                                            <label class="input-group-text" for="inputGroupFile01"><i class="bi bi-upload"></i></label>
                                            <input type="file" class="form-control" id="inputGroupFile01" name="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label for="last-name-column">Description</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
                                    <br>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-outline-primary me-1 mb-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary me-1 mb-1">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
