@extends('layouts.header')
@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">
                    Data Table
                </h5>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="table1_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div id="table1_filter" class="dataTables_filter"><label>Search:<input type="search"
                                            class="form-control form-control-sm" placeholder="" aria-controls="table1">
                                        <br></label></div>
                            </div>
                        </div>
                        <div class="row dt-row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table dataTable no-footer" id="table1" aria-describedby="table1_info">
                                        <thead class="text-center">
                                            <tr>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Image
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Name
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Brand
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 160px;">
                                                    Series Number
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Size
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Material
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Category
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">
                                                    Created Year
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">Added By
                                                </th>

                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="table1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending"
                                                    style="width: 130px;">Action
                                                </th>

                                            </tr>
                                        </thead>
                                        @foreach ($item as $t)
                                            <tbody class="text-center">
                                                <tr class="odd">
                                                    <td><img src="{{ $t->image }}" alt="{{ $t->name }}"></td>
                                                    <td class="sorting_1">{{ $t->name }}</td>
                                                    <td>{{ $t->merk }}</td>
                                                    <td>{{ $t->no_seri }}</td>
                                                    <td>{{ $t->size }}</td>
                                                    <td>{{ $t->material }}</td>
                                                    <td>{{ $t->category->name }}</td>
                                                    <td>{{ $t->created_years }}</td>
                                                    <td>{{ $t->user->name }}</td>
                                                    <td class="">
                                                        <div class="d-flex">
                                                            <a href="#"
                                                            class="btn btn-outline-primary me-1 mb-1">Detail</a>
                                                            <a href="/edit"
                                                            class="btn btn-outline-warning me-1 mb-1">Edit</a>
                                                            <a href="#"
                                                            class="btn btn-outline-danger me-1 mb-1">Delete</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="row py-4">
                                <div class="col-sm-12 col-md-5">
                                    <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">
                                        <ul class="pagination pagination-primary">
                                            @php
                                                $firstPage = $item->onFirstPage();
                                                $current = $item->currentPage();
                                                $lastPage = $item->onlastPage();
                                            @endphp
                                            @if ($current = $firstPage)
                                                <li class="page-item active disabled"><a class="page-link" href="#">{{ $item->currentPage() }}</a></li> 
                                                <li class="page-item"><a class="page-link" href="{{ $item->url(20) }}">{{ $item->lastPage() }}</a></li>
                                                <li class="page-item"><a class="page-link" href="{{ $item->nextPageUrl() }}">Next</a></li>    
                                            @elseif($current = $lastPage)
                                                <li class="page-item"><a class="page-link" href="{{ $item->previousPageUrl() }}">Prev</a></li>
                                                <li class="page-item"><a class="page-link" href="{{ $item->url(1) }}">{{ $item->hasPages() }}</a></li>  
                                                <li class="page-item active disabled"><a class="page-link" href="#">{{ $item->currentPage() }}</a></li> 
                                            @else
                                                <li class="page-item"><a class="page-link" href="{{ $item->previousPageUrl() }}">Prev</a></li>
                                                <li class="page-item"><a class="page-link" href="{{ $item->url(1) }}">{{ $item->hasPages() }}</a></li>  
                                                <li class="page-item active disabled"><a class="page-link" href="#">{{ $item->currentPage() }}</a></li> 
                                                <li class="page-item"><a class="page-link" href="{{ $item->url(20) }}">{{ $item->lastPage() }}</a></li>
                                                <li class="page-item"><a class="page-link" href="{{ $item->nextPageUrl() }}">Next</a></li>  
                                            @endif
                                            </ul>
                                        Showing {{ $item->currentPage() }} of {{ $item->lastPage() }} page's
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection
