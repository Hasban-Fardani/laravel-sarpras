<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="./assets/compiled/svg/favicon.svg" type="image/x-icon">
    <link rel="shortcut icon" href="" type="image/png">

    <link rel="stylesheet" href="assets/extensions/sweetalert2/sweetalert2.min.css">

    <link rel="stylesheet" href="./assets/compiled/css/app.css">
    <link rel="stylesheet" href="./assets/compiled/css/app-dark.css">
    <link rel="stylesheet" href="./assets/compiled/css/iconly.css">

    <title>Dashboard</title>
</head>

<body>
    <script src="./assets/static/js/initTheme.js"></script>

    <div id="app">
        <div id="sidebar">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header position-relative">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="logo">
                            <a href="index.html"><img src="./pict/logo-sarpras.png" alt="Logo" width="100%"></a>
                        </div>
                        <div class="theme-toggle d-flex gap-2  align-items-center mt-2">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                aria-hidden="true" role="img" class="iconify iconify--system-uicons" width="20"
                                height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 21 21">
                                <g fill="none" fill-rule="evenodd" stroke="currentColor" stroke-linecap="round"
                                    stroke-linejoin="round">
                                    <path
                                        d="M10.5 14.5c2.219 0 4-1.763 4-3.982a4.003 4.003 0 0 0-4-4.018c-2.219 0-4 1.781-4 4c0 2.219 1.781 4 4 4zM4.136 4.136L5.55 5.55m9.9 9.9l1.414 1.414M1.5 10.5h2m14 0h2M4.135 16.863L5.55 15.45m9.899-9.9l1.414-1.415M10.5 19.5v-2m0-14v-2"
                                        opacity=".3"></path>
                                    <g transform="translate(-210 -1)">
                                        <path d="M220.5 2.5v2m6.5.5l-1.5 1.5"></path>
                                        <circle cx="220.5" cy="11.5" r="4"></circle>
                                        <path d="m214 5l1.5 1.5m5 14v-2m6.5-.5l-1.5-1.5M214 18l1.5-1.5m-4-5h2m14 0h2">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                            <div class="form-check form-switch fs-6">
                                <input class="form-check-input  me-0" type="checkbox" id="toggle-dark"
                                    style="cursor: pointer">
                                <label class="form-check-label"></label>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                aria-hidden="true" role="img" class="iconify iconify--mdi" width="20"
                                height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                                <path fill="currentColor"
                                    d="m17.75 4.09l-2.53 1.94l.91 3.06l-2.63-1.81l-2.63 1.81l.91-3.06l-2.53-1.94L12.44 4l1.06-3l1.06 3l3.19.09m3.5 6.91l-1.64 1.25l.59 1.98l-1.7-1.17l-1.7 1.17l.59-1.98L15.75 11l2.06-.05L18.5 9l.69 1.95l2.06.05m-2.28 4.95c.83-.08 1.72 1.1 1.19 1.85c-.32.45-.66.87-1.08 1.27C15.17 23 8.84 23 4.94 19.07c-3.91-3.9-3.91-10.24 0-14.14c.4-.4.82-.76 1.27-1.08c.75-.53 1.93.36 1.85 1.19c-.27 2.86.69 5.83 2.89 8.02a9.96 9.96 0 0 0 8.02 2.89m-1.64 2.02a12.08 12.08 0 0 1-7.8-3.47c-2.17-2.19-3.33-5-3.49-7.82c-2.81 3.14-2.7 7.96.31 10.98c3.02 3.01 7.84 3.12 10.98.31Z">
                                </path>
                            </svg>
                        </div>
                        <div class="sidebar-toggler  x">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i
                                    class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Reports</li>

                        <li class="sidebar-item active ">
                            @can('isAdmin')
                            <a href="/admin" class='sidebar-link'>
                            @elsecan('isOperator')
                                <a href="/operator" class='sidebar-link'>
                                @endcan
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                                </a>
                            </a>
                        </li>

                        <li class="sidebar-title">Forms &amp; Tables</li>

                        {{-- Menu 1: manage item --}}
                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-box-fill"></i>
                                <span>Manage Items</span>
                            </a>
                            <ul class="submenu ">
                                <li class="submenu-item  ">
                                    <a href="/add" class="submenu-link">Add item</a>
                                </li>
                                <li class="submenu-item">
                                    <a href="/itemDetail" class="submenu-link">Item detail</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Menu 2: borrowing items --}}
                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-diagram-3"></i>
                                <span>Borrowing Items</span>
                            </a>
                            <ul class="submenu ">
                                <li class="submenu-item">
                                    <a href="#" class="submenu-link">-</a>
                                </li>

                                <li class="submenu-item  ">
                                    <a href="#" class="submenu-link">-</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Menu 3: manage user --}}
                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-person-fill-gear"></i>
                                <span>Manage Users</span>
                            </a>
                            <ul class="submenu ">
                                <li class="submenu-item  ">
                                    <a href="/lendRequest" class="submenu-link">Lending request</a>
                                </li>
                                <li class="submenu-item  ">
                                    <a href="#" class="submenu-link">-</a>
                                </li>
                                <li class="submenu-item  ">
                                    <a href="#" class="submenu-link">-</a>
                                </li>
                            </ul>
                        </li>

                        <li class="sidebar-title">Profile</li>

                        <li class="sidebar-item has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-person-check-fill"></i>
                                @guest
                                @else
                                    <span>{{ Auth::user()->name }}</span>
                                @endguest
                            </a>

                            <ul class="submenu ">
                                @guest
                                    @if (Route::has('login'))
                                        <li class="submenu-item  ">
                                            <a class="submenu-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                    @endif
                                    @if (Route::has('register'))
                                        <li class="submenu-item  ">
                                            <a class="submenu-link"
                                                href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                    <li class="submenu-item"></li>
                                    <li class="submenu-item">
                                        <a class="submenu-link" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                @endguest
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="main">
        <header class="mb-3">
            <a href="#" class="burger-btn d-block d-xl-none">
                <i class="bi bi-justify fs-3"></i>
            </a>
        </header>
        <div class="page-heading">
            @can('isAdmin')
                <h3>Admin Dashboard</h3>
            @elsecan('isOperator')
                <h3>Operator Dashboard</h3>
            @endcan
        </div>
        <div class="page-content mb-5">
            <div class="row">
                <div class="col-12 col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>

        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="float-start">
                    <p>2024 © Sarpras</p>
                </div>
                <div class="float-end">
                    <p>Crafted with <span class="text-danger"><i class="bi bi-heart-fill icon-mid"></i></span>
                        by <a href="https://saugi.me">Sarpras Team</a></p>
                </div>
            </div>
        </footer>
    </div>

    <!-- Script -->
    <script src="./assets/static/js/components/dark.js"></script>
    <script src="./assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js"></script>


    <script src="./assets/compiled/js/app.js"></script>



    <!-- Need: Apexcharts -->
    <script src="./assets/extensions/apexcharts/apexcharts.min.js"></script>
    <script src="./assets/static/js/pages/dashboard.js"></script>
    <script src="./assets/extensions/sweetalert2/sweetalert2.min.js"></script>
    <script src="./assets/static/js/pages/sweetalert2.js"></script>
</body>

</html>
