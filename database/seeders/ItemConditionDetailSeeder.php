<?php

namespace Database\Seeders;

use App\Models\ItemConditionDetail;
use Illuminate\Database\Seeder;

class ItemConditionDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ItemConditionDetail::factory(10)->create();
    }
}
