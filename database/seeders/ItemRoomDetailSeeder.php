<?php

namespace Database\Seeders;

use App\Models\ItemRoomDetail;
use Illuminate\Database\Seeder;

class ItemRoomDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        ItemRoomDetail::factory(100)->create();
    }
}
