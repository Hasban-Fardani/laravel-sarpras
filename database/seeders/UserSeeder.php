<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
      User::create([
        "image" => "https://cdn-icons-png.freepik.com/512/1144/1144760.png",
        "name" => "Admin ganteng",
        "email" => "admin@localhost",
        "password" => bcrypt("password"),
        "role" => "admin"
      ]);

      User::create([
        "image" => "https://cdn-icons-png.freepik.com/512/1144/1144760.png",
        "name" => "Operator kece",
        "email" => "operator@localhost",
        "password" => bcrypt("password"),
        "role" => "operator"
      ]);

      User::create([
        "image" => "https://cdn-icons-png.freepik.com/512/1144/1144760.png",
        "name" => "User cakep",
        "email" => "user@localhost",
        "password" => bcrypt("password"),
        "role" => "user"
      ]);

      User::factory(20)->create();
    }
}
