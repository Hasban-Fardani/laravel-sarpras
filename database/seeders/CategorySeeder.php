<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Category::create([
            'name' => 'Elektronik',
            'is_disposable' => false,
        ]);

        Category::create([
            'name' => 'ATK sekali pakai',
        ]);

        Category::create([
            'name' => 'furniture',
            'is_disposable' => false,
        ]);
    }
}
