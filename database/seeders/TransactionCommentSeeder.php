<?php

namespace Database\Seeders;

use App\Models\TransactionComment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TransactionCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        TransactionComment::factory(100)->create();
    }
}
