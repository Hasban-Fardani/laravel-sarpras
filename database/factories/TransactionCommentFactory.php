<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class TransactionCommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'from_user_id' => fake()->numberBetween(1, 23),
            'to_user_id' => fake()->numberBetween(1, 23),
            'transaction_id' => fake()->numberBetween(1, 50),
            'comment' => fake()->sentence(),
        ];
    }
}
