<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'description' => fake()->sentence(5),
            'type' => fake()->randomElement(['add', 'update', 'delete', 'borrow', 'return']),
            'count' => fake()->numberBetween(1, 100),
            'user_id' => fake()->numberBetween(1, 20),
            'item_room_detail_id' => fake()->numberBetween(1, 100),
        ];
    }
}
