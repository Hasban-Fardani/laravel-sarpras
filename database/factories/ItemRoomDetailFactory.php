<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ItemRoomDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //
            'room_id' => fake()->numberBetween(1, 3),
            'item_id' => fake()->numberBetween(1, 20),
            'count_total' => fake()->numberBetween(5, 25),
            'good_total' => fake()->numberBetween(1, 5),
            'less_total' => fake()->numberBetween(1, 5),
            'broken_total' => fake()->numberBetween(1, 5),
            'borrowed_total' => fake()->numberBetween(1, 5),
            'available_total' => fake()->numberBetween(1, 5),
        ];
    }
}
