<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ItemStatus>
 */
class ItemConditionDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'image' => fake()->image(),
            'code' => fake()->word(),
            'name' => fake()->name(),
            'status' => fake()->randomElement(['good', 'less', 'broken']),
            'item_id' => fake()->numberBetween(1, 20),
            'room_id' => fake()->numberBetween(1, 10),
        ];
    }
}
