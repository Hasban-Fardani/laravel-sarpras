<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'description' => fake()->sentence(10),
            'merk' => fake()->sentence(1),
            'no_seri' => fake()->randomNumber(6, true),
            'material' => fake()->randomElement(['kayu', 'plastik', 'besi', 'aluminium', 'kertas', 'kaca']),
            'created_years' => fake()->numberBetween(2020, 2023),
            'category_id' => fake()->numberBetween(1, 3),
            'user_id' => fake()->numberBetween(1, 2),
        ];
    }
}
