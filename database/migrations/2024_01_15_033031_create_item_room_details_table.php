<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item_room_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('count_total');
            $table->unsignedBigInteger('good_total');
            $table->unsignedBigInteger('less_total');
            $table->unsignedBigInteger('broken_total');
            $table->unsignedBigInteger('borrowed_total');
            $table->unsignedBigInteger('available_total');

            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('item_id')->references('id')->on('items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item_room_details');
    }
};
