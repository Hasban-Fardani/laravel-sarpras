<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('item_condition_details', function (Blueprint $table) {
            $table->id();
            $table->string('code')->comment('item code');
            $table->string('image')->comment('image url');
            $table->string('name')->unique();
            $table->enum('status', ['good', 'less', 'broken'])->default('good');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('room_id');

            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('item_condition_details');
    }
};
