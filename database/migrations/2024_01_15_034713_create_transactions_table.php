<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('count');
            $table->enum('type', ['add','update','delete', 'borrow', 'return']);
            $table->enum('status', ['confirmed', 'pending', 'rejected']);
            $table->unsignedBigInteger('item_room_detail_id');
            $table->unsignedBigInteger('user_id')->comment('this is user who created transaction');
            $table->timestamps();

            $table->foreign('item_room_detail_id')->references('id')->on('item_room_details');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
