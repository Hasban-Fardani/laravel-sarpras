<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function itemRoomDetail() {
        return $this->belongsTo(ItemRoomDetail::class);
    }

    public function transactionComments() {
        return $this->hasMany(TransactionComment::class);
    }
}
