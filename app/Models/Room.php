<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at'];

    public function items() {
        return $this->hasMany(Item::class);
    }

    public function itemRoomDetails() {
        return $this->hasMany(ItemRoomDetail::class);
    }

    public function itemConditionDetails() {
        return $this->hasMany(ItemConditionDetail::class);
    }
}
