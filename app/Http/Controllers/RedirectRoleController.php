<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class RedirectRoleController extends Controller
{
    public function __invoke()
    {
        if(Auth::user()->role == 'admin') {
            return redirect('/admin');
        } elseif (Auth::user()->role == 'operator') {
            return redirect('/operator');
        }
        return redirect('/user');
    }
}
