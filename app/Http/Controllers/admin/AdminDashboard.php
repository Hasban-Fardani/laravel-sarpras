<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Models\User;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

class AdminDashboard extends Controller
{
    //
    public function __invoke()
    {
        $category = Category::all();
        $item = Item::With('category', 'user')->paginate(5);
        return view('admin.item-detail', compact('item', 'category'));
    }
}
