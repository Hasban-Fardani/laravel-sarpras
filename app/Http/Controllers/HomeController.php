<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
        return view('admin.add-item');
    }

    public function edit()
    {
        return view('admin.edit-item');
    }

    public function lendRequest()
    {
        return view('admin.lend-request');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        if (Gate::allows('isAdmin')) {
            return view('admin.dashboard');
        } elseif (Gate::allows('isOperator')) {
            return view('admin.dashboard');
        } else {
            return view('beranda');
        }
     }
}
