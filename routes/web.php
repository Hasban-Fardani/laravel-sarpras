<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\admin\AdminDashboard;
use App\Http\Controllers\RedirectRoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', function () {
    if (Auth::check()){
        return redirect('check');
    }
    return redirect('login');
});

Route::get('/check', RedirectRoleController::class);

// admin
Route::group(['prefix' => '/admin', 'middleware' => ['can:isAdmin']], function () {
    Route::get('/', [HomeController::class, 'index']);
});

// operator
Route::group(['prefix' => '/operator', 'middleware' => ['can:isOperator']], function () {
    Route::get('/', [HomeController::class, 'index']);
});

// user
Route::group(['prefix' => '/user', 'middleware' => ['can:isUser']], function () {
    Route::get('/', [HomeController::class, 'index']);
});

// silfi
Route::get('/add', [HomeController::class, 'add'])->name('add');
Route::get('/edit', [HomeController::class, 'edit'])->name('edit');
Route::get('/lendRequest', [HomeController::class, 'lendRequest'])->name('lendRequest');

Route::get('/itemDetail', AdminDashboard::class)->name('datatable');